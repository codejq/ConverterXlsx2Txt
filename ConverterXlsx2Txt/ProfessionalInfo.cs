﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConverterXlsx2Txt
{
    public class ProfessionalInfo
    {
        const string TAB = "\t";
        public ProfessionalInfo() { }

        #region Default properties

        string firstName;
        string lastName;
        int age;
        string countryCode;
        string countryName;
        string countryPhone;
        string profession;


        public string FirstName
        {
            get { return firstName; }
            set { firstName = Capitalize(value); }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = Capitalize(value); }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }

        public string CountryName
        {
            get { return countryName; }
            set { countryName = Capitalize(value); }
        }

        public string CountryPhone
        {
            get { return countryPhone; }
            set { countryPhone = value;}
        }

        public string Profession
        {
            get { return profession; }
            set { profession = Capitalize(value); }
        }



        #endregion

        #region Other Properties

        public string FullName
        {
            get { return string.Concat(FirstName, " ", LastName); }
        }

        public string AgeWithTitle
        {
            get
            {
                return string.Concat(age, " years old");
            }
        }

        public string Birthdate
        {

            get
            {
                DateTime birthdate = RandomBirthDateByAge(age);
                return birthdate.ToString("dd-MM-yyyy");
            }
        }
        #endregion

        private string Capitalize(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            return string.Concat(char.ToUpper(s[0]), s.ToLower().Substring(1));
        }

        private DateTime RandomBirthDateByAge(int age)
        {
            
            int month = Random.Shared.Next(1, 12);
            int year = DateTime.Now.Year - age;
            if(month == 2)
            {
                int day = Random.Shared.Next(1, 29);
                try
                {
                    return new DateTime(year, month, day);
                }
                catch(Exception ex)
                {
                    return RandomBirthDateByAge(age);
                }
            }
            else
            {
                int day = Random.Shared.Next(1, 31);
                return new DateTime(year, month, day);
            }
        }

        public string getCSVLine()
        {
            return string.Concat(FullName, TAB, Birthdate, TAB, AgeWithTitle, TAB, CountryName, TAB, Profession, Environment.NewLine);
        }

        public string getCSVLineHeader()
        {
            return string.Concat(nameof(FullName), TAB, nameof(Birthdate), TAB, nameof(Age), TAB, nameof(CountryName), TAB, nameof(Profession), Environment.NewLine);
        }

    }
}
