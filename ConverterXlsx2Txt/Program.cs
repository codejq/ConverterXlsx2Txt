﻿using ConverterXlsx2Txt;
using FastExcel;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Text;

const int NUMBER_OF_TASKS = 25;

string processDirectory;
const string dirPathFile = "Outputs";
const string prefixOutputFile = "Professionals_";
const string extCsvFile = ".csv";

Cell[] headerColumnsNames;
Stream fileXls = new FileStream("Resources\\data.xlsx", FileMode.Open);
ConcurrentBag<ProfessionalInfo> bagRows = new ConcurrentBag<ProfessionalInfo>();

using (FastExcel.FastExcel managerXls = new FastExcel.FastExcel(fileXls))
{
    Stopwatch stopwatch = new Stopwatch();
    Stopwatch processWatch = new Stopwatch();
    processWatch.Start();
    stopwatch.Start();

    // Reading all data from file.
    Worksheet worksheet = managerXls.Read("data");
    Row[] rows = worksheet.Rows.ToArray();

    // Getting names of column header.
    headerColumnsNames = rows[0].Cells.ToArray();
    Console.WriteLine("File Readed in {0} seconds", stopwatch.Elapsed.TotalSeconds);
    stopwatch.Restart();

    // Converting row to object.
    Parallel.For(1, rows.Length, (i, state) =>
    {
        ProfessionalInfo info;
        ConvertRowToProfessionalInfo(rows[i], out info);
        if (info != null)
            bagRows.Add(info);
    });

    Console.WriteLine("Converter Array to DTO adding in ConcurrentBag {0} seconds", stopwatch.Elapsed.TotalSeconds);
    stopwatch.Restart();

    // Clear array and call the garbage collector.
    Array.Clear(rows);
    GC.Collect();

    Console.WriteLine("Garbage Collector {0} seconds", stopwatch.Elapsed.TotalSeconds);
    stopwatch.Restart();

    processDirectory = DateTime.Now.ToString("yyyyMMddTHHmmss.fffffff");
    calculateNumberOfTasks();
    Console.WriteLine("Task assigned in: {0} seconds", stopwatch.Elapsed.TotalSeconds);
    processWatch.Stop();
    Console.WriteLine("Process time finished in {0} seconds", processWatch.Elapsed.TotalSeconds);
}

void calculateNumberOfTasks()
{
    // Calculating the number of task to generate.
    int numberOfRows = bagRows.Count;
    Console.WriteLine("Count of Rows: {0}", numberOfRows);

    int blockSize = numberOfRows / NUMBER_OF_TASKS;
    int blockResidual = numberOfRows % NUMBER_OF_TASKS;
    int numberOfTaskProcess = blockResidual > 0 ? NUMBER_OF_TASKS + 1 : NUMBER_OF_TASKS;

    Console.WriteLine("# Number of rows: {0}, # Of tasks {1}", numberOfRows, NUMBER_OF_TASKS);
    Console.WriteLine("# Block size: {0}, # Block residue {1}", blockSize, blockResidual);

    for (int taskNumber = 1; taskNumber <= numberOfTaskProcess; taskNumber++, blockSize = taskNumber < numberOfTaskProcess ? blockSize : blockResidual)
    {
        Console.WriteLine("TASK#{0} -blockSize: {1}", taskNumber, blockSize);
        processRowsBlock(blockSize);
    }
}

void processRowsBlock(int blockSize)
{
    Task.Run(() =>
    {

        string newDirectory = Path.Combine(dirPathFile, processDirectory);
        if (!Directory.Exists(newDirectory))
        {
            Directory.CreateDirectory(newDirectory);
            string fileName = string.Concat(prefixOutputFile, Task.CurrentId, extCsvFile);
            string pathOutputFile = Path.Combine(newDirectory, fileName);
            if (!File.Exists(pathOutputFile))
            {
                FileStream outputFile = File.Create(pathOutputFile);

                // Write lines in CSV
                for (int index = 0; index < blockSize; index++)
                {
                    ProfessionalInfo info;
                    bagRows.TryTake(out info);

                    if (index > 0)
                    {
                        outputFile.Write(Encoding.UTF8.GetBytes(info.getCSVLine()));
                    }
                    else
                    {
                        // Write headers name of columns in CSV
                        outputFile.Write(Encoding.UTF8.GetBytes(info.getCSVLineHeader()));
                    }
                }
                outputFile.Close();
            }
        }

       
    });
}

// Convert Row Type to Object.
void ConvertRowToProfessionalInfo(Row row, out ProfessionalInfo info)
{
    try
    {
        info = new ProfessionalInfo();
        Cell[] cells = row.Cells.ToArray();
        info.FirstName = Convert.ToString(cells[0].Value);
        info.LastName = Convert.ToString(cells[1].Value);
        info.Age = Convert.ToInt32(cells[2].Value);
        info.Profession = Convert.ToString(cells[3].Value);
        info.CountryCode = Convert.ToString(cells[4].Value);
        info.CountryName = Convert.ToString(cells[5].Value);
        info.CountryPhone = Convert.ToString(cells[6].Value);
    }
    catch (Exception ex)
    {
        info = null;
        Console.WriteLine($"{ex.Message}");
    }
}
